#include <iostream>
#include <fstream>
#include <string>

//Arg class to manage all the args
class Args
{
public:
    std::string fileName;
    std::string commandType;
};

void processArgs(int argc, char **argv, Args *args);

int countLines(std::istream &readFile, Args *args);

int countCharacters(std::istream &readFile, Args *args);

int main(int argc, char **argv)
{
    //create and arg to process all the args
    Args args;
    processArgs(argc, argv, &args);

    //Opens the file
    std::ifstream inputFile(args.fileName);

    //Check if file is good and has the correct arguement
    if (args.commandType == "-l" && inputFile.good())
    {
        countLines(inputFile, &args);
        inputFile.close();
    }
    else if (args.commandType == "-c" && inputFile.good())
    {
        countCharacters(inputFile, &args);
        inputFile.close();
    }
    else
    {
        std::cout << "Inavlid Arguement" << std::endl;
    }

    return EXIT_SUCCESS;
}

//Function to count number of lines according to wc program
int countLines(std::istream &readFile, Args *args)
{
    std::string output = "";
    int numberOfLines = 0;

    while (std::getline(readFile, output))
    {
        ++numberOfLines;
    }

    std::cout << "Number of Lines in " << args->fileName << ": " << numberOfLines << std::endl;
    return numberOfLines;
}

//Function to count number of characters according to wc program
int countCharacters(std::istream &readFile, Args *args)
{
    std::string output = "";
    int numberOfCharacters = 0;
    int numberOfLines = 0;

    while (std::getline(readFile, output))
    {
        numberOfCharacters += output.size();
        ++numberOfLines;
    }

    //NOTE: The wc program also counts a line as a character
    std::cout << "Number of Characters in " << args->fileName << ": " << numberOfCharacters + numberOfLines << std::endl;

    return numberOfCharacters;
}

//Processing args
void processArgs(int argc, char **argv, Args *args)
{
    //Second arguement - command type
    if (argc >= 2)
    {
        std::string command(argv[1]);
        args->commandType = command;
    }
    else
    {
        args->commandType = "";
    }

    //Third arguement - file name
    if (argc >= 3)
    {
        std::string filename(argv[2]);
        args->fileName = filename;
    }
    else
    {
        args->fileName = "";
    }
}